File: 20081009.gpx
    Length 2D: 317.694km
    Length 3D: 317.902km
    Moving time: 08:01:40
    Stopped time: 03:14:35
    Max speed: 30.93m/s = 111.35km/h
    Total uphill: 2412.32m
    Total downhill: 2168.70m
    Started: 2008-10-09 16:14:55
    Ended: 2008-10-10 03:59:55
    Points: 3665
    Avg distance between points: 90.08m

    Track #0, Segment #0
        Length 2D: 302.535km
        Length 3D: 302.716km
        Moving time: 04:28:22
        Stopped time: 00:48:10
        Max speed: 30.93m/s = 111.35km/h
        Total uphill: 2312.60m
        Total downhill: 2091.65m
        Started: 2008-10-09 16:14:55
        Ended: 2008-10-09 21:31:27
        Points: 2364
        Avg distance between points: 127.98m

    Track #1, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.005km
        Moving time: 00:00:03
        Stopped time: 00:00:40
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.90m
        Total downhill: 2.89m
        Started: 2008-10-09 21:32:00
        Ended: 2008-10-09 21:32:43
        Points: 4
        Avg distance between points: 0.00m

    Track #2, Segment #0
        Length 2D: 0.211km
        Length 3D: 0.219km
        Moving time: 00:02:48
        Stopped time: 00:00:03
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 4.74m
        Total downhill: 1.64m
        Started: 2008-10-09 21:35:28
        Ended: 2008-10-09 21:38:19
        Points: 20
        Avg distance between points: 10.56m

    Track #3, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: 00:00:01
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.06m
        Total downhill: 0.00m
        Started: 2008-10-09 21:46:45
        Ended: 2008-10-09 21:46:46
        Points: 2
        Avg distance between points: 0.00m

    Track #4, Segment #0
        Length 2D: 0.307km
        Length 3D: 0.307km
        Moving time: 00:02:37
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.74m
        Total downhill: 2.85m
        Started: 2008-10-09 21:47:00
        Ended: 2008-10-09 21:49:37
        Points: 4
        Avg distance between points: 76.73m

    Track #5, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-09 21:49:53
        Ended: 2008-10-09 21:49:53
        Points: 1
        Avg distance between points: 0.00m

    Track #6, Segment #0
        Length 2D: 0.508km
        Length 3D: 0.508km
        Moving time: 00:08:05
        Stopped time: 00:08:04
        Max speed: 1.80m/s = 6.50km/h
        Total uphill: 1.92m
        Total downhill: 5.04m
        Started: 2008-10-09 21:50:04
        Ended: 2008-10-09 22:06:13
        Points: 38
        Avg distance between points: 13.36m

    Track #7, Segment #0
        Length 2D: 3.708km
        Length 3D: 3.714km
        Moving time: 00:58:59
        Stopped time: 01:29:38
        Max speed: 1.57m/s = 5.64km/h
        Total uphill: 44.62m
        Total downhill: 40.19m
        Started: 2008-10-09 22:06:24
        Ended: 2008-10-10 00:35:01
        Points: 281
        Avg distance between points: 13.19m

    Track #8, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.004km
        Moving time: n/a
        Stopped time: 00:00:17
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 3.53m
        Total downhill: 0.00m
        Started: 2008-10-10 00:35:25
        Ended: 2008-10-10 00:35:42
        Points: 2
        Avg distance between points: 0.00m

    Track #9, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 00:36:10
        Ended: 2008-10-10 00:36:10
        Points: 1
        Avg distance between points: 0.00m

    Track #10, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 00:36:42
        Ended: 2008-10-10 00:36:42
        Points: 1
        Avg distance between points: 0.00m

    Track #11, Segment #0
        Length 2D: 2.981km
        Length 3D: 2.982km
        Moving time: 00:54:40
        Stopped time: 00:14:43
        Max speed: 2.11m/s = 7.61km/h
        Total uphill: 8.46m
        Total downhill: 10.59m
        Started: 2008-10-10 00:37:18
        Ended: 2008-10-10 01:46:41
        Points: 310
        Avg distance between points: 9.62m

    Track #12, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 01:47:05
        Ended: 2008-10-10 01:47:05
        Points: 1
        Avg distance between points: 0.00m

    Track #13, Segment #0
        Length 2D: 0.231km
        Length 3D: 0.231km
        Moving time: 00:04:42
        Stopped time: 00:00:23
        Max speed: 2.31m/s = 8.30km/h
        Total uphill: 0.27m
        Total downhill: 0.55m
        Started: 2008-10-10 01:47:44
        Ended: 2008-10-10 01:52:49
        Points: 24
        Avg distance between points: 9.61m

    Track #14, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 01:53:20
        Ended: 2008-10-10 01:53:20
        Points: 1
        Avg distance between points: 0.00m

    Track #15, Segment #0
        Length 2D: 0.060km
        Length 3D: 0.060km
        Moving time: 00:00:32
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.05m
        Total downhill: 0.13m
        Started: 2008-10-10 01:53:22
        Ended: 2008-10-10 01:53:54
        Points: 5
        Avg distance between points: 12.05m

    Track #16, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 01:54:15
        Ended: 2008-10-10 01:54:15
        Points: 1
        Avg distance between points: 0.00m

    Track #17, Segment #0
        Length 2D: 0.363km
        Length 3D: 0.363km
        Moving time: 00:02:39
        Stopped time: 00:00:07
        Max speed: 2.73m/s = 9.83km/h
        Total uphill: 0.88m
        Total downhill: 0.35m
        Started: 2008-10-10 01:57:31
        Ended: 2008-10-10 02:00:17
        Points: 24
        Avg distance between points: 15.12m

    Track #18, Segment #0
        Length 2D: 1.499km
        Length 3D: 1.500km
        Moving time: 00:24:35
        Stopped time: 00:24:40
        Max speed: 2.22m/s = 7.98km/h
        Total uphill: 7.60m
        Total downhill: 3.61m
        Started: 2008-10-10 02:00:49
        Ended: 2008-10-10 02:50:04
        Points: 158
        Avg distance between points: 9.48m

    Track #19, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 02:50:13
        Ended: 2008-10-10 02:50:13
        Points: 1
        Avg distance between points: 0.00m

    Track #20, Segment #0
        Length 2D: 0.242km
        Length 3D: 0.242km
        Moving time: 00:01:34
        Stopped time: 00:00:37
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.27m
        Total downhill: 0.13m
        Started: 2008-10-10 02:50:31
        Ended: 2008-10-10 02:52:42
        Points: 8
        Avg distance between points: 30.24m

    Track #21, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 02:53:15
        Ended: 2008-10-10 02:53:15
        Points: 1
        Avg distance between points: 0.00m

    Track #22, Segment #0
        Length 2D: 0.202km
        Length 3D: 0.202km
        Moving time: 00:05:29
        Stopped time: 00:00:29
        Max speed: 1.28m/s = 4.60km/h
        Total uphill: 0.70m
        Total downhill: 0.67m
        Started: 2008-10-10 02:53:24
        Ended: 2008-10-10 02:59:22
        Points: 34
        Avg distance between points: 5.93m

    Track #23, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 02:59:55
        Ended: 2008-10-10 02:59:55
        Points: 1
        Avg distance between points: 0.00m

    Track #24, Segment #0
        Length 2D: 0.925km
        Length 3D: 0.925km
        Moving time: 00:14:15
        Stopped time: 00:01:30
        Max speed: 1.92m/s = 6.90km/h
        Total uphill: 1.12m
        Total downhill: 4.65m
        Started: 2008-10-10 03:00:03
        Ended: 2008-10-10 03:15:48
        Points: 104
        Avg distance between points: 8.89m

    Track #25, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 03:16:20
        Ended: 2008-10-10 03:16:20
        Points: 1
        Avg distance between points: 0.00m

    Track #26, Segment #0
        Length 2D: 0.164km
        Length 3D: 0.164km
        Moving time: 00:02:37
        Stopped time: 00:00:10
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.16m
        Total downhill: 0.62m
        Started: 2008-10-10 03:16:22
        Ended: 2008-10-10 03:19:09
        Points: 18
        Avg distance between points: 9.11m

    Track #27, Segment #0
        Length 2D: 0.705km
        Length 3D: 0.706km
        Moving time: 00:06:08
        Stopped time: 00:01:53
        Max speed: 4.85m/s = 17.48km/h
        Total uphill: 11.62m
        Total downhill: 0.14m
        Started: 2008-10-10 03:19:41
        Ended: 2008-10-10 03:27:42
        Points: 49
        Avg distance between points: 14.39m

    Track #28, Segment #0
        Length 2D: 0.049km
        Length 3D: 0.049km
        Moving time: 00:00:08
        Stopped time: 00:00:36
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.16m
        Total downhill: 0.00m
        Started: 2008-10-10 03:28:13
        Ended: 2008-10-10 03:28:57
        Points: 3
        Avg distance between points: 16.28m

    Track #29, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 03:29:29
        Ended: 2008-10-10 03:29:29
        Points: 1
        Avg distance between points: 0.00m

    Track #30, Segment #0
        Length 2D: 0.798km
        Length 3D: 0.798km
        Moving time: 00:00:52
        Stopped time: 00:00:23
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 1.18m
        Total downhill: 0.01m
        Started: 2008-10-10 03:29:31
        Ended: 2008-10-10 03:30:46
        Points: 7
        Avg distance between points: 114.04m

    Track #31, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 03:31:16
        Ended: 2008-10-10 03:31:16
        Points: 1
        Avg distance between points: 0.00m

    Track #32, Segment #0
        Length 2D: 0.506km
        Length 3D: 0.506km
        Moving time: 00:01:30
        Stopped time: 00:00:14
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 1.18m
        Total downhill: 0.01m
        Started: 2008-10-10 03:34:45
        Ended: 2008-10-10 03:36:29
        Points: 17
        Avg distance between points: 29.78m

    Track #33, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 03:36:52
        Ended: 2008-10-10 03:36:52
        Points: 1
        Avg distance between points: 0.00m

    Track #34, Segment #0
        Length 2D: 1.701km
        Length 3D: 1.701km
        Moving time: 00:21:05
        Stopped time: 00:01:57
        Max speed: 2.47m/s = 8.88km/h
        Total uphill: 9.56m
        Total downhill: 2.97m
        Started: 2008-10-10 03:36:53
        Ended: 2008-10-10 03:59:55
        Points: 176
        Avg distance between points: 9.66m

File: 20081010.gpx
    Length 2D: 215.514km
    Length 3D: 215.658km
    Moving time: 03:03:58
    Stopped time: 00:07:44
    Max speed: 31.04m/s = 111.74km/h
    Total uphill: 2390.06m
    Total downhill: 2434.82m
    Started: 2008-10-10 04:00:08
    Ended: 2008-10-11 00:33:05
    Points: 1924
    Avg distance between points: 113.07m

    Track #0, Segment #0
        Length 2D: 0.050km
        Length 3D: 0.050km
        Moving time: 00:01:04
        Stopped time: 00:00:17
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.22m
        Total downhill: 0.27m
        Started: 2008-10-10 04:00:08
        Ended: 2008-10-10 04:01:29
        Points: 9
        Avg distance between points: 5.52m

    Track #1, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-10 04:01:56
        Ended: 2008-10-10 04:01:56
        Points: 1
        Avg distance between points: 0.00m

    Track #2, Segment #0
        Length 2D: 0.076km
        Length 3D: 0.076km
        Moving time: 00:01:22
        Stopped time: 00:00:11
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.24m
        Total downhill: 0.24m
        Started: 2008-10-10 04:02:04
        Ended: 2008-10-10 04:03:37
        Points: 12
        Avg distance between points: 6.31m

    Track #3, Segment #0
        Length 2D: 210.420km
        Length 3D: 210.523km
        Moving time: 02:46:38
        Stopped time: 00:03:07
        Max speed: 31.04m/s = 111.74km/h
        Total uphill: 2275.63m
        Total downhill: 2361.70m
        Started: 2008-10-10 21:24:03
        Ended: 2008-10-11 00:13:48
        Points: 1768
        Avg distance between points: 119.02m

    Track #4, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.015km
        Moving time: 00:00:19
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 8.98m
        Total downhill: 0.00m
        Started: 2008-10-11 00:13:49
        Ended: 2008-10-11 00:14:08
        Points: 8
        Avg distance between points: 0.00m

    Track #5, Segment #0
        Length 2D: 4.968km
        Length 3D: 4.994km
        Moving time: 00:14:35
        Stopped time: 00:04:09
        Max speed: 12.09m/s = 43.53km/h
        Total uphill: 104.98m
        Total downhill: 72.62m
        Started: 2008-10-11 00:14:21
        Ended: 2008-10-11 00:33:05
        Points: 126
        Avg distance between points: 39.43m

File: 20081011.gpx
    Length 2D: 574.127km
    Length 3D: 574.650km
    Moving time: 09:54:15
    Stopped time: 03:22:35
    Max speed: 32.69m/s = 117.67km/h
    Total uphill: 5070.14m
    Total downhill: 5050.61m
    Started: 2008-10-11 13:33:24
    Ended: 2008-10-12 03:59:55
    Points: 4937
    Avg distance between points: 116.31m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-11 13:33:24
        Ended: 2008-10-11 13:33:24
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-11 13:34:04
        Ended: 2008-10-11 13:34:04
        Points: 1
        Avg distance between points: 0.00m

    Track #2, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-11 14:42:58
        Ended: 2008-10-11 14:42:58
        Points: 1
        Avg distance between points: 0.00m

    Track #3, Segment #0
        Length 2D: 574.127km
        Length 3D: 574.650km
        Moving time: 09:54:15
        Stopped time: 03:22:35
        Max speed: 32.69m/s = 117.67km/h
        Total uphill: 5070.14m
        Total downhill: 5050.61m
        Started: 2008-10-11 14:43:05
        Ended: 2008-10-12 03:59:55
        Points: 4934
        Avg distance between points: 116.36m

File: 20081012.gpx
    Length 2D: 112.624km
    Length 3D: 112.641km
    Moving time: 01:10:36
    Stopped time: 00:03:30
    Max speed: 33.18m/s = 119.44km/h
    Total uphill: 419.92m
    Total downhill: 626.46m
    Started: 2008-10-12 04:00:03
    Ended: 2008-10-12 05:14:09
    Points: 561
    Avg distance between points: 200.76m

    Track #0, Segment #0
        Length 2D: 112.624km
        Length 3D: 112.641km
        Moving time: 01:10:36
        Stopped time: 00:03:30
        Max speed: 33.18m/s = 119.44km/h
        Total uphill: 419.92m
        Total downhill: 626.46m
        Started: 2008-10-12 04:00:03
        Ended: 2008-10-12 05:14:09
        Points: 561
        Avg distance between points: 200.76m

File: 20081013.gpx
    Length 2D: 0.000km
    Length 3D: 0.000km
    Moving time: n/a
    Stopped time: n/a
    Max speed: 0.00m/s = 0.00km/h
    Total uphill: 0.00m
    Total downhill: 0.00m
    Started: 2008-10-14 01:05:00
    Ended: 2008-10-14 01:05:00
    Points: 1
    Avg distance between points: 0.00m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-14 01:05:00
        Ended: 2008-10-14 01:05:00
        Points: 1
        Avg distance between points: 0.00m

File: 20081017.gpx
    Length 2D: 267.079km
    Length 3D: 267.202km
    Moving time: 04:03:22
    Stopped time: 01:44:35
    Max speed: 31.96m/s = 115.05km/h
    Total uphill: 971.27m
    Total downhill: 614.81m
    Started: 2008-10-17 22:12:01
    Ended: 2008-10-18 04:02:19
    Points: 1263
    Avg distance between points: 2784.11m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-17 22:12:01
        Ended: 2008-10-17 22:12:01
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 25.185km
        Length 3D: 25.288km
        Moving time: 01:15:00
        Stopped time: 00:55:14
        Max speed: 17.96m/s = 64.66km/h
        Total uphill: 282.47m
        Total downhill: 107.88m
        Started: 2008-10-17 22:14:07
        Ended: 2008-10-18 00:24:21
        Points: 390
        Avg distance between points: 64.58m

    Track #2, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-18 00:24:30
        Ended: 2008-10-18 00:24:30
        Points: 1
        Avg distance between points: 0.00m

    Track #3, Segment #0
        Length 2D: 241.894km
        Length 3D: 241.915km
        Moving time: 02:48:22
        Stopped time: 00:49:21
        Max speed: 31.96m/s = 115.05km/h
        Total uphill: 688.80m
        Total downhill: 506.92m
        Started: 2008-10-18 00:24:36
        Ended: 2008-10-18 04:02:19
        Points: 871
        Avg distance between points: 277.72m

File: 20081018.gpx
    Length 2D: 54.865km
    Length 3D: 55.106km
    Moving time: 06:04:08
    Stopped time: 03:44:39
    Max speed: 8.23m/s = 29.65km/h
    Total uphill: 985.82m
    Total downhill: 988.81m
    Started: 2008-10-18 15:22:16
    Ended: 2008-10-19 01:13:00
    Points: 2119
    Avg distance between points: 26.12m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-18 15:22:16
        Ended: 2008-10-18 15:22:16
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 25.660km
        Length 3D: 25.747km
        Moving time: 02:21:51
        Stopped time: 01:42:12
        Max speed: 8.00m/s = 28.82km/h
        Total uphill: 582.01m
        Total downhill: 270.95m
        Started: 2008-10-18 15:22:58
        Ended: 2008-10-18 19:27:01
        Points: 928
        Avg distance between points: 27.65m

    Track #2, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.002km
        Moving time: 00:00:01
        Stopped time: 00:07:28
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 1.41m
        Total downhill: 0.00m
        Started: 2008-10-18 19:28:11
        Ended: 2008-10-18 19:35:40
        Points: 3
        Avg distance between points: 0.00m

    Track #3, Segment #0
        Length 2D: 29.204km
        Length 3D: 29.358km
        Moving time: 03:42:16
        Stopped time: 01:54:59
        Max speed: 8.23m/s = 29.65km/h
        Total uphill: 402.40m
        Total downhill: 717.86m
        Started: 2008-10-18 19:35:45
        Ended: 2008-10-19 01:13:00
        Points: 1187
        Avg distance between points: 24.60m

File: 20081019.gpx
    Length 2D: 610.865km
    Length 3D: 611.176km
    Moving time: 09:26:37
    Stopped time: 03:18:07
    Max speed: 32.31m/s = 116.33km/h
    Total uphill: 5233.29m
    Total downhill: 4140.40m
    Started: 2008-10-19 14:03:31
    Ended: 2008-10-20 02:51:08
    Points: 4874
    Avg distance between points: 125.33m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-19 14:03:31
        Ended: 2008-10-19 14:03:31
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 433.774km
        Length 3D: 433.937km
        Moving time: 05:25:09
        Stopped time: 01:52:02
        Max speed: 32.31m/s = 116.33km/h
        Total uphill: 4118.82m
        Total downhill: 2321.89m
        Started: 2008-10-19 14:04:00
        Ended: 2008-10-19 21:21:11
        Points: 2913
        Avg distance between points: 148.91m

    Track #2, Segment #0
        Length 2D: 177.092km
        Length 3D: 177.239km
        Moving time: 04:01:28
        Stopped time: 01:26:05
        Max speed: 28.51m/s = 102.65km/h
        Total uphill: 1114.47m
        Total downhill: 1818.51m
        Started: 2008-10-19 21:23:35
        Ended: 2008-10-20 02:51:08
        Points: 1960
        Avg distance between points: 90.35m

File: 20081020.gpx
    Length 2D: 624.616km
    Length 3D: 624.947km
    Moving time: 10:09:42
    Stopped time: 05:16:37
    Max speed: 32.32m/s = 116.36km/h
    Total uphill: 4608.60m
    Total downhill: 4988.38m
    Started: 2008-10-20 13:55:19
    Ended: 2008-10-21 05:25:32
    Points: 5169
    Avg distance between points: 120.85m

    Track #0, Segment #0
        Length 2D: 459.720km
        Length 3D: 459.986km
        Moving time: 08:04:33
        Stopped time: 04:46:13
        Max speed: 32.32m/s = 116.36km/h
        Total uphill: 3026.02m
        Total downhill: 3309.47m
        Started: 2008-10-20 13:55:19
        Ended: 2008-10-21 02:46:05
        Points: 3825
        Avg distance between points: 120.19m

    Track #1, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-21 02:46:38
        Ended: 2008-10-21 02:46:38
        Points: 1
        Avg distance between points: 0.00m

    Track #2, Segment #0
        Length 2D: 0.430km
        Length 3D: 0.430km
        Moving time: 00:05:17
        Stopped time: n/a
        Max speed: 3.30m/s = 11.87km/h
        Total uphill: 1.47m
        Total downhill: 1.85m
        Started: 2008-10-21 02:49:28
        Ended: 2008-10-21 02:54:45
        Points: 38
        Avg distance between points: 11.31m

    Track #3, Segment #0
        Length 2D: 164.466km
        Length 3D: 164.531km
        Moving time: 01:59:52
        Stopped time: 00:30:24
        Max speed: 31.07m/s = 111.86km/h
        Total uphill: 1581.11m
        Total downhill: 1677.07m
        Started: 2008-10-21 02:55:16
        Ended: 2008-10-21 05:25:32
        Points: 1305
        Avg distance between points: 126.03m

File: 20081021.gpx
    Length 2D: 241.869km
    Length 3D: 242.613km
    Moving time: 08:06:59
    Stopped time: 05:25:22
    Max speed: 31.72m/s = 114.20km/h
    Total uphill: 3916.75m
    Total downhill: 3283.37m
    Started: 2008-10-21 14:50:37
    Ended: 2008-10-22 04:31:33
    Points: 4435
    Avg distance between points: 54.56m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-21 14:50:37
        Ended: 2008-10-21 14:50:37
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 35.263km
        Length 3D: 35.293km
        Moving time: 00:42:25
        Stopped time: 00:34:40
        Max speed: 18.50m/s = 66.60km/h
        Total uphill: 697.00m
        Total downhill: 357.99m
        Started: 2008-10-21 14:51:35
        Ended: 2008-10-21 16:08:40
        Points: 456
        Avg distance between points: 77.33m

    Track #2, Segment #0
        Length 2D: 80.874km
        Length 3D: 81.512km
        Moving time: 05:57:04
        Stopped time: 04:28:10
        Max speed: 17.92m/s = 64.51km/h
        Total uphill: 1496.50m
        Total downhill: 1803.55m
        Started: 2008-10-21 16:10:09
        Ended: 2008-10-22 02:35:23
        Points: 2861
        Avg distance between points: 28.27m

    Track #3, Segment #0
        Length 2D: 125.731km
        Length 3D: 125.808km
        Moving time: 01:27:30
        Stopped time: 00:22:32
        Max speed: 31.72m/s = 114.20km/h
        Total uphill: 1723.26m
        Total downhill: 1121.82m
        Started: 2008-10-22 02:41:31
        Ended: 2008-10-22 04:31:33
        Points: 1117
        Avg distance between points: 112.56m

File: 20081022.gpx
    Length 2D: 500.506km
    Length 3D: 501.570km
    Moving time: 09:34:00
    Stopped time: 03:38:06
    Max speed: 31.85m/s = 114.64km/h
    Total uphill: 4989.34m
    Total downhill: 4759.90m
    Started: 2008-10-22 14:50:02
    Ended: 2008-10-23 04:02:45
    Points: 5216
    Avg distance between points: 95.96m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-22 14:50:02
        Ended: 2008-10-22 14:50:02
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 500.506km
        Length 3D: 501.570km
        Moving time: 09:34:00
        Stopped time: 03:38:06
        Max speed: 31.85m/s = 114.64km/h
        Total uphill: 4989.34m
        Total downhill: 4759.90m
        Started: 2008-10-22 14:50:39
        Ended: 2008-10-23 04:02:45
        Points: 5215
        Avg distance between points: 95.97m

File: 20081023.gpx
    Length 2D: 104.939km
    Length 3D: 105.575km
    Moving time: 04:44:29
    Stopped time: 03:05:49
    Max speed: 26.80m/s = 96.48km/h
    Total uphill: 1072.52m
    Total downhill: 1706.72m
    Started: 2008-10-23 15:29:28
    Ended: 2008-10-24 02:53:04
    Points: 2306
    Avg distance between points: 47.05m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-23 15:29:28
        Ended: 2008-10-23 15:29:28
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 0.022km
        Length 3D: 0.022km
        Moving time: 00:00:13
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.66m
        Total downhill: 0.00m
        Started: 2008-10-23 15:30:08
        Ended: 2008-10-23 15:30:21
        Points: 3
        Avg distance between points: 7.39m

    Track #2, Segment #0
        Length 2D: 36.341km
        Length 3D: 36.380km
        Moving time: 00:50:15
        Stopped time: 00:26:53
        Max speed: 26.80m/s = 96.48km/h
        Total uphill: 178.96m
        Total downhill: 630.91m
        Started: 2008-10-23 15:30:53
        Ended: 2008-10-23 16:48:01
        Points: 479
        Avg distance between points: 75.87m

    Track #3, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: 00:00:01
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-23 20:18:30
        Ended: 2008-10-23 20:18:31
        Points: 2
        Avg distance between points: 0.00m

    Track #4, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-23 20:19:21
        Ended: 2008-10-23 20:19:21
        Points: 1
        Avg distance between points: 0.00m

    Track #5, Segment #0
        Length 2D: 68.576km
        Length 3D: 69.173km
        Moving time: 03:54:01
        Stopped time: 02:38:55
        Max speed: 16.19m/s = 58.27km/h
        Total uphill: 892.90m
        Total downhill: 1075.81m
        Started: 2008-10-23 20:20:08
        Ended: 2008-10-24 02:53:04
        Points: 1820
        Avg distance between points: 37.68m

File: 20081024.gpx
    Length 2D: 288.402km
    Length 3D: 288.682km
    Moving time: 07:56:16
    Stopped time: 05:15:14
    Max speed: 35.36m/s = 127.29km/h
    Total uphill: 2167.47m
    Total downhill: 3252.46m
    Started: 2008-10-24 15:44:18
    Ended: 2008-10-25 04:56:07
    Points: 3569
    Avg distance between points: 80.89m

    Track #0, Segment #0
        Length 2D: 261.286km
        Length 3D: 261.522km
        Moving time: 06:50:37
        Stopped time: 02:36:39
        Max speed: 35.36m/s = 127.29km/h
        Total uphill: 2038.42m
        Total downhill: 3162.22m
        Started: 2008-10-24 15:44:18
        Ended: 2008-10-25 01:11:34
        Points: 3164
        Avg distance between points: 82.58m

    Track #1, Segment #0
        Length 2D: 27.115km
        Length 3D: 27.160km
        Moving time: 01:05:39
        Stopped time: 02:38:35
        Max speed: 21.81m/s = 78.53km/h
        Total uphill: 129.05m
        Total downhill: 90.24m
        Started: 2008-10-25 01:11:53
        Ended: 2008-10-25 04:56:07
        Points: 405
        Avg distance between points: 66.95m

File: 20081025.gpx
    Length 2D: 9.664km
    Length 3D: 9.665km
    Moving time: 00:16:46
    Stopped time: 00:01:59
    Max speed: 16.45m/s = 59.24km/h
    Total uphill: 6.57m
    Total downhill: 38.12m
    Started: 2008-10-25 13:00:35
    Ended: 2008-10-25 13:20:14
    Points: 81
    Avg distance between points: 121.69m

    Track #0, Segment #0
        Length 2D: 0.000km
        Length 3D: 0.000km
        Moving time: n/a
        Stopped time: n/a
        Max speed: 0.00m/s = 0.00km/h
        Total uphill: 0.00m
        Total downhill: 0.00m
        Started: 2008-10-25 13:00:35
        Ended: 2008-10-25 13:00:35
        Points: 1
        Avg distance between points: 0.00m

    Track #1, Segment #0
        Length 2D: 9.664km
        Length 3D: 9.665km
        Moving time: 00:16:46
        Stopped time: 00:01:59
        Max speed: 16.45m/s = 59.24km/h
        Total uphill: 6.57m
        Total downhill: 38.12m
        Started: 2008-10-25 13:01:29
        Ended: 2008-10-25 13:20:14
        Points: 80
        Avg distance between points: 120.80m

