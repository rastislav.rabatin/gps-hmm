# GPS Tracks

The goal of this task is to train HMM which can decide based on the GPS data if
the person with GPS device was standing *still* (not moving), *walking* or
driving a *car*. The GPS data which I used are in directory `data`.  They are
store in GPX format which is basically special format of XML.  The GPS device
stores for each track point four value -- latitude, longitude, elevation and
timestamp.

## Data Exploration

The data set is composed of 40 120 track points in total.

### Time Differences

At first, I looked at the distribution of timestamp differences between two
track points. The timestamps are recorded in seconds. Therefore the minimum
difference is one second. The median is two seconds. The third quartile is six
seconds. Unfortunately, we also have some anomalies -- time differences greater
than one minute. The various changes in time differance mean that we will have
to be careful when computing features for HMM. Probably, every feature has to be
normalized by time.

### Track names

I found out that the track names are unique. Therefore, I will refer to them as
unique identifiers. I also found out that every track is composed of only one
track segment. GPX format allows to have multiple segments stores in one GPS
track.

### Track lengths

The median of track lengths is 8 but the mean is 514. The longest track has 5
215 track points. Therefore I guess that the really short tracks might have a
lot of errors. These short tracks might correspond to situations when the person
lost GPS signal very quickly. When we filter all GPS tracks that are shorter
than 100 then we still end up with 39 678 track points, in total. In other
words, we still have 40k track points. We did not reduce our data set too much.
However, we went down from 78 tracks to 26 tracks which means that we had quite
a lot of short tracks.

## Feature Selection

### Speed

First candidate for a good feature is speed. I have used `gpxpy` library for
parsing and processing GPS tracks. This library also provides a method for
computing speed. If two track points are far away the it uses `haversine`
function otherwise it uses faster approximation of this function.  Their
implementation of `haversine` is based on
[this](http://www.movable-type.co.uk/scripts/latlong.html).

When we look at the speeds we can notice that we have got some speeds which are
probably not possible on a normal car. I have decided that all speeds above 40
m/s which is 144 km/h are errors in GPS. I found that I have only nine
measurements above this threshold.

It seems that all the tracks are recorded in the U.S. The speed limit on a
freeway in most of the states is 70 miles per hour which is approx. 112 km/h but
there are couple states which have speed limit 80 miles per hour which is 129
km/h (36 m/s).  The histogram of all speeds lower than 40 m/s is below. We can
notice three peaks at around 1 m/s, 27 m/s and 31 m/s. Last two peaks correspond
to the person traveling by a car.

![](plots/speeds_lower_than_40.png)

We also want to differentiate between standing *still* and *walking*. See the
zoomed in histogram to speeds lower 3 m/s. The average person usually moves at
the speed of 1.4 m/s. We can notice two peaks in this histogram. The left peak
clearly corresponds to "standing *still*" and the second peak corresponds to
*walking*. When we look at the speeds then we can notice that we never have pure
zero speed. The slowest speed is 0.0014 m/s.

![](plots/speeds_lower_than_3.png)

#### Speeds and Movement of The Person

We are basically interested in three states of the person's movement -- *still*
(standing still), *walking* and *car* (moving by a car) state.

It looks like the speeds in the *still* state can be modelled by one exponential
or gaussian distribution. The emissions at *walking* state seems to be centered
around 1.4 m/s. They also look like one Gaussian distribution. The problem comes
with *car* state. It looks like we are going to need multiple Gaussian states. I
have tried to take logarithm of the speeds but it still did not make the higher
speeds to look like one Gaussian. See the zoomed in portion of histogram which
corresponds to speeds greater than 5 m/s. We can see two clear peaks - two
narrow Gaussian distributions.  The speeds in the range 5-25 m/s look like
multiple Gaussian distributions. When we really simplify the distribution on
interval 5-15 m/s then we could say that it is uniform distribution.  We do not
really want to use uniform distribution because it has sharp edges.  We probably
want a smoother distribution. We can also notice one really small and narrow
peak at 35 m/s (126 km/h).

![](plots/speeds_greater_than_5.png)

### Normalized Differences between Latitude and Longitude

My second idea was to use normalized differences between latitude and longitude
of two consecutive points. The difference would be normalized by time. In other
words, this quantity kind of corresponds to speed in the direction of
"coordinate". This strategy did not turn out to be successful. See the plot
below. I hoped to see rings which would correspond to individual states but it
seems like the data are not so easily separable.  Zooming in does not help.

![](plots/norm_diffs.png)

### Change in Direction of Movement

When the person is *still* then logically the person does not have any speed.
The problem is that GPS is not accurate and thus we can observe the change of
GPS coordinates even if the person did not move. One of the hints that we were
provided are that the person was hiking when he/she was using GPS device. We can
also observe this from the GPS track when we visualize them on google maps.
Hikers are usually heading in one direction unless they got lost. Then they
might be moving back and forth. When they are still then GPS is reporting some
random changes in location which means that the changes in direction are
probably completely random compare to changes in direction for the *walking*
state.

#### Azimuth

One of the most common way to describe the direction in geography is azimuth.
See the histogram of azimuths below. Zero degrees corresponds to direction
directly heading towards north. The formula for azimuth was taken from
[here](https://github.com/jonblack/cmpgpx/blob/master/geo.py). In addition, the
azimuth was divide be time. It seems like the azimuth distribution can be
modelled by exponential distribution.

![](plots/azimuth.png)

#### Change in Azimuth

But we are not really interested in azimuth. We are more interested in change of
direction of movement which is change of azimuth. The plot of changes in azimuth
can be seen below.

![](plots/azimuth_changes.png)

#### Other Ideas

Another interesting feature might be acceleration -- change in speed.

## Hidden Markov Models (HMM)

### HMM on speeds

As I mentioned above, the *still* state and *walking* state can be described
by only one emission distribution. The emission distribution of *car* state
is more complicated. We need more Gaussian states. At first, I tried to model
*still* state by exponential distribution but it did not turn out to be good 
decision. Therefore I switched to normal distribution and I got more reasonable
results. For *walking* state, I decided to use normal distribution. For the 
*car* state I decided to use multiple state. I tried three states. Every state 
should correspond to different speeds - slow speed of car, moderate and fast 
(freeway).

#### Training HMM

Both of the models were trained using EM algorithm. The implementation of the
algorithm can be found in `gps-hmm/hmm.py` and the script for training is
`gps-hmm/train_hmm.py`. Both of the models were trained for 30 iterations.

#### Google Maps Visualization

I have used `gmplot` library which generates from python html files with 
javascript which directly calls Google Maps API. All the maps can be found
[here](https://rastislav.rabatin.gitlab.io/gps-hmm/).

##### Legend:

- Green - still state
- Blue - walking
- Yellow - low speed of car
- White - moderate speed of car
- Magenta - high speed of car
- Red - Error in GPS

#### HMM with Exponential Distribution for Still State

![](plots/five_hmm_likelihood.png)

[Training log](logs/five_hmm.log)

[Google Maps Visualization](https://rastislav.rabatin.gitlab.io/gps-hmm/exp_five)

[Pickled model](models/five_iter30.pickle)

[Viterbi Paths](models/five_iter30_viterbi.txt)

#### HMM with Normal Distribution for Still State

![](plots/gauss_five_likelihood.png)

[Training log](logs/gauss_five.log)

[Google Maps Visualization](https://rastislav.rabatin.gitlab.io/gps-hmm/gauss_five)

[Pickled model](models/gauss_five_iter30.pickle)

[Viterbi Paths](models/gauss_five_iter30_viterbi.txt)

#### Other Ideas

I could try to use discrete HMM. The speeds can be quantized -- split into 
buckets of regular size. The problem with this approach is that we quickly 
increase the number of parameters which we need to train.

Another idea might be to try more HMM states to describe the speed distribution
for movement of car.

Another thing that might help to differentiate between *still* and *walking*
is to incorporate azimuth changes into the model. We could try to have two
dimensional Gaussian in every state -- speed and change in azimuth. Or maybe
the azimuth can be better modelled by exponential distribution. The real problem
with azimuth is that it can only value from some range such as 0-360 degrees.
We could experiment with distributions like
[this](https://en.wikipedia.org/wiki/Circular_distribution) or again use
quantization on degrees.

## Software

Everything was implemented using `python3`. All the used libraries are listed in
`requirements.txt`. You can install them by running
`pip3 install -r requirements.txt`. All the code is store in `gps-hmm` 
directory.
