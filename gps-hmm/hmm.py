import sys
import pickle
import logging
import scipy.stats
import numpy as np

from time import time
from numpy import log, exp, abs
from scipy.misc import logsumexp
from pathos.multiprocessing import ProcessingPool


ZERO = -np.inf
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


def is_zero(x):
    return x == ZERO


def log_sum(x, y):
    if is_zero(x):
        return y

    if is_zero(y):
        return x

    return np.logaddexp(x, y)


class ExponentialState:
    """HMM state with emissions from exponential distribution

    Median of Exp is ln(2)/lambda => beta = median/ln(2).

    Attributes:
        state_id (int): id of the state in HMM.
        beta (float): parameter of exponential distribution.
                      beta is the mean of the distribution.
                      beta = 1/lambda
    """

    def __init__(self, state_id, beta):
        self.state_id = state_id
        self.beta = beta

    def density(self, x):
        return scipy.stats.expon.pdf(x, scale=self.beta)

    def log_density(self, x):
        res = self.density(x)
        if res == 0:
            return ZERO

        return log(res)

    def update_params(self, gammas, seqs):
        """This method is used in EM algorithm."""
        mean_num, mean_denom = ContinuousHMM.get_mean_estimate(
            self.state_id, gammas, seqs)

        self.beta = exp(mean_num - mean_denom)

    def __repr__(self):
        return "Exp(λ={0:.4f})".format(1/self.beta)

class UnivariateGaussianState:
    """ HMM state with emissions from gaussian distribution

    99.7 % -> mean +- 3*sigma
    95 %   -> mean +- 2*sigma
    68 %   -> mean +- sigma

    Attributes:
        mean (float): mean of the distribution.
        sigma (float): standard deviation of the Gaussian.
        state_id (int): id of the state in HMM.
    """

    def __init__(self, state_id, mean, sigma):
        self.mean = mean
        self.sigma = sigma
        self.state_id = state_id

    def density(self, x):
        """Return PDF value at point x."""
        return scipy.stats.norm.pdf(x, loc=self.mean, scale=self.sigma)

    def log_density(self, x):
        """Return log PDF value at point x."""
        res = self.density(x)
        if res == 0:
            return ZERO

        return log(res)

    def update_params(self, gammas, seqs):
        """This method is used in EM algorithm."""
        state_id = self.state_id

        # Estimate mean.
        mean_num, mean_denom = ContinuousHMM.get_mean_estimate(
            self.state_id, gammas, seqs)

        self.mean = exp(mean_num - mean_denom)

        # Estimate sigma.
        variance_num = ZERO
        for seq_id, seq in enumerate(seqs):
            for t in range(len(seq)):
                variance_num = log_sum(variance_num,
                                       gammas[seq_id][t][state_id] +
                                       2 * log(abs(seq[t] - self.mean)))

        # Again, convert sigma back from log space.
        self.sigma = np.sqrt(exp(variance_num - mean_denom))

    def __repr__(self):
        return "N({0:.4f}, {1:.4f}^2)".format(self.mean, self.sigma)


class ContinuousHMM:
    """ Continuous Hidden Markov model.

    All probabilities are kept in log space throughout the computation.

    Attributes:
        n_states (int): number of states.
        transitions (np.array): state transition probability matrix.
        emission_distrib (list): emission distribution for every state.
        initial_transitions (np.array): initial transition probabilities.
    """

    def __init__(self, transitions, emission_distrib, initial_transitions):
        n_states = transitions.shape[0]
        self.n_states = n_states

        assert transitions.shape == (n_states, n_states)
        assert len(emission_distrib) == n_states
        assert len(initial_transitions) == n_states

        self.transitions = log(transitions)
        self.emission_distrib = emission_distrib
        self.initial_transitions = log(initial_transitions)

    def forward(self, seq):
        """ Compute forward[t][q] = P(seq[:t+1], X_t = q)

        Args:
            seq: a vector representing a sequence of observed outputs.

        Returns:
            np.array of shape (len(seq), n_states).
        """
        seq_len = len(seq)
        n_states = self.n_states
        res = np.full((seq_len, n_states), ZERO)

        for state in range(n_states):
            res[0][state] = self.initial_transitions[state] + \
                            self.emission_distrib[state].log_density(seq[0])

        for pos in range(1, seq_len):
            for state in range(n_states):
                for prev_state in range(n_states):
                    res[pos][state] = log_sum(res[pos][state],
                                              res[pos - 1][prev_state] +
                                              self.transitions[prev_state][state] +
                                              self.emission_distrib[state]
                                                  .log_density(seq[pos]))

        return res

    def backward(self, seq):
        """ Compute backward[t][q] = P(seq[t+1:] | X_t = q)

        Args:
            seq: a vector representing a sequence of observed outputs.

        Returns:
            np.array of shape (len(seq), n_states).
        """
        seq_len = len(seq)
        n_states = self.n_states
        res = np.full((seq_len, n_states), ZERO)

        for state in range(n_states):
            res[seq_len - 1][state] = log(1)

        for pos in reversed(range(0, seq_len - 1)):
            for state in range(n_states):
                for next_state in range(n_states):
                    res[pos][state] = log_sum(res[pos][state],
                                              res[pos + 1][next_state] +
                                              self.transitions[state][next_state] +
                                              self.emission_distrib[next_state]
                                                  .log_density(seq[pos + 1]))

        return res

    @staticmethod
    def get_gamma(forward_matrix, backward_matrix):
        """ Compute gamma matrix

        gamma[t][state] = P(X_t = state | observation sequence)

        Args:
            forward_matrix: a matrix returned from the forward function
            backward_matrix: a matrix returned from the backward function

        Returns:
            np.array of shape (len(seq), n_states).
        """
        res = np.array(forward_matrix) + np.array(backward_matrix)
        res -= logsumexp(res, axis=1, keepdims=True)
        res[np.isnan(res)] = ZERO

        return res

    def get_xi(self, seq, forward_matrix, backward_matrix):
        """ Compute xi matrix

        xi[t][state_from][state_to] =
            P(X_t = state_from, X_{t+1} = state_to | observation sequence)

        Args:
            seq (list): sequence of observations.
            forward_matrix: a matrix returned from the forward function
            backward_matrix: a matrix returned from the backward function

        Returns:
            np.array of shape (len(seq)-1, n_states, n_states)
        """
        n_states = self.n_states
        seq_len = len(seq)

        xis = np.full((seq_len - 1, n_states, n_states), ZERO)
        for pos in range(seq_len - 1):
            for from_state in range(n_states):
                for to_state in range(n_states):
                    xis[pos][from_state][to_state] = log_sum(
                        xis[pos][from_state][to_state],
                        forward_matrix[pos, from_state] +
                        self.transitions[from_state, to_state] +
                        self.emission_distrib[to_state].log_density(seq[pos + 1]) +
                        backward_matrix[pos + 1, to_state])

            margin = logsumexp(xis[pos], axis=(0, 1))
            xis[pos] -= margin
            xis[pos][np.isnan(xis[pos])] = ZERO

        return xis

    def viterbi(self, seq):
        """ Return the most probable sequence of states

        Args:
            seq: a vector representing a sequence of observed outputs.

        Returns:
            list of states
        """
        seq_len = len(seq)
        n_states = self.n_states
        V = np.full((seq_len, n_states), ZERO)
        S = np.full((seq_len, n_states), -1, dtype=int)

        for state in range(n_states):
            V[0][state] = self.initial_transitions[state] + \
                          self.emission_distrib[state].log_density(seq[0])

        for pos in range(1, seq_len):
            for state in range(n_states):
                for prev_state in range(n_states):
                    prob = V[pos - 1][prev_state] + \
                           self.transitions[prev_state][state] + \
                           self.emission_distrib[state].log_density(seq[pos])

                    if V[pos][state] < prob:
                        V[pos][state] = prob
                        S[pos][state] = prev_state

        path = [-1 for _ in range(seq_len)]
        cur_state = np.argmax(V[seq_len - 1])
        for pos in reversed(range(seq_len)):
            path[pos] = cur_state
            cur_state = S[pos][cur_state]

        assert cur_state == -1

        return path

    def e_step(self, seqs):
        """Run E-step of EM algorithm

        Args:
            seqs (list): list of observation sequences.

        Returns:
            (gammas, xis) - aggregated gamma and xi matrix for all sequences.
        """
        pool = ProcessingPool()

        def fn(seq):
            forward = self.forward(seq)
            backward = self.backward(seq)

            return (self.get_gamma(forward, backward),
                    self.get_xi(seq, forward, backward))

        res = pool.map(fn, seqs)
        return [x[0] for x in res], [x[1] for x in res]

    def m_step(self, gammas, xis, seqs):
        """ Run M-step of EM algorithm

        This method directly updates parameters of this model.

        Args:
            gammas (list): list of gamma matrices for every sequence.
            xis (list): list of xi matrices for every sequence.
            seqs (list): list of observation sequences.
        """
        n_states = self.n_states

        # Update transition matrix.
        for state_from in range(n_states):
            for state_to in range(n_states):
                num = ZERO
                denom = ZERO
                for seq_id, seq in enumerate(seqs):
                    for pos in range(len(seq) - 1):
                        num = log_sum(
                            num, xis[seq_id][pos][state_from][state_to])
                        denom = log_sum(
                            denom, gammas[seq_id][pos][state_from])

                self.transitions[state_from][state_to] = num - denom

        # Update initial transitions.
        for state in range(n_states):
            self.initial_transitions[state] = logsumexp(
                [gammas[seq_id][0][state] for seq_id in range(len(seqs))])

        self.initial_transitions -= logsumexp(self.initial_transitions)
        self.initial_transitions[np.isnan(self.initial_transitions)] = ZERO

        # Update emission distributions.
        for state in self.emission_distrib:
            state.update_params(gammas, seqs)

    def fit(self, seqs, n_iters=15):
        """ Fit the model.

        This method directly modifies the parameters of the model.

        Args:
            seqs (list): list of observation sequences.
            n_iters (int): number of iterations of EM algorithm.
        """
        np.set_printoptions(threshold=np.inf)

        for iteration in range(1, n_iters + 1):
            logging.info("Iter: %d; Running E-step.", iteration)
            start = time()
            gammas, xis = self.e_step(seqs)
            logging.info("Iter: %d; E-step took: %f sec", iteration,
                         time() - start)

            logging.info("Iter: %d; Running M-step.", iteration)
            start = time()
            self.m_step(gammas, xis, seqs)
            logging.info("Iter: %d; M-step took: %f sec", iteration,
                         time() - start)

            logging.info("Iter: %d; NEW PARAMS:\n%s", iteration, self)

            logging.info("Iter: %d; Computing model likelihood.", iteration)
            start = time()
            likelihood = self.model_likelihood(seqs)
            logging.info("Iter: %d; Log Likelihood: %f", iteration, likelihood)
            logging.info("Iter: %d; Likelihood comp. took: %f sec", iteration,
                         time() - start)

    def model_likelihood(self, seqs):
        """ Compute log likelihood of the model given the sequences

        Args:
            seqs (list): list of observation sequences.

        Returns:
            log likelihood of the model
        """
        pool = ProcessingPool()

        def fn(seq):
            return logsumexp(self.forward(seq)[-1][:])

        return sum(pool.map(fn, seqs))

    @staticmethod
    def get_mean_estimate(state_id, gammas, seqs):
        """ Compute mean estimate for a state

        Args:
            state_id (int): state from HMM
            gammas (list): list of np.array-s - see ContinousHMMM.get_gamma().
            seqs (list): list of training sequences.

        Returns:
            num (float): numerator of mean estimate in log space.
            denom (float): denominator of mean estimate in log space.
        """
        # Compute mean estimate.
        mean_num = ZERO
        mean_denom = ZERO
        for seq_id, seq in enumerate(seqs):
            for t in range(len(seq)):
                mean_num = log_sum(mean_num,
                                   gammas[seq_id][t][state_id] + log(seq[t]))
                mean_denom = log_sum(mean_denom, gammas[seq_id][t][state_id])

        return mean_num, mean_denom

    def __str__(self):
        return "TRANSITIONS:\n{}\n" \
               "INITIAL TRANSITIONS:\n{}\n" \
               "EMISSIONS:\n{}".format(exp(self.transitions),
                                       exp(self.initial_transitions),
                                       self.emission_distrib)

    def save(self, filename):
        pickle.dump(self, open(filename, "wb"))

    @staticmethod
    def load(filename):
        return pickle.load(open(filename, "rb"))
