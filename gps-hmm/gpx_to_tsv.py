#!/usr/bin/env python3
"""
Read all GPX files from DIR to pd.DataFrame and merge them to one DataFrame.
Afterwards, write this DataFrame to all_tracks.tsv.
"""

import os
import gpxpy
import pandas as pd


DIR = '../data'


def parse_gpx(filename):
    """Return `gpxpy.GPX` object from a file."""
    gpx = gpxpy.parse(open("{}/{}".format(DIR, filename), 'r'))
    gpx.name = filename if gpx.name is None else gpx.name
    return gpx


def get_df_for_segment(segment):
    """Convert gpxpy.GPXTrackSegment to pd.DataFrame.
    The cols of the data frame are: latitude, longitude, elevation, time.
    """
    seg_dict = {}

    for point in segment.points:
        seg_dict[point.time] = [point.latitude, point.longitude,
                                point.elevation, point.time]

    res = pd.DataFrame(data=seg_dict)
    res = res.T
    res.columns = ['latitude', 'longitude', 'elevation', 'time']
    res.reset_index(drop=True, inplace=True)
    return res


def get_df_for_track(track):
    """Convert gpxpy.GPXTrack to pd.DataFrame.
    The cols of the data frame are: latitude, longitude, elevation, time, seg_idx.
    """
    res = pd.DataFrame()
    for idx, segment in enumerate(track.segments):
        df = get_df_for_segment(segment)
        df['seg_idx'] = idx
        res = res.append(df)

    res.reset_index(drop=True, inplace=True)
    return res


def get_df_for_gpx(gpx):
    """Convert gpxpy.GPX to pd.DataFrame.
    The cols of the data frame are: latitude, longitude, elevation, time,
    seg_idx, track_name.
    """
    res = pd.DataFrame()
    for track in gpx.tracks:
        df = get_df_for_track(track)
        df['track_name'] = track.name
        res = res.append(df)

    res.reset_index(drop=True, inplace=True)
    return res


def get_df_for_all_files_in(directory):
    """Read all GPX files from the directory and return pd.DataFrame.
    The cols of the data frame are: latitude, longitude, elevation, time,
    seg_idx, track_name and filename.
    """
    res = pd.DataFrame()
    for gpx_file in os.listdir(directory):
        if gpx_file.split('.')[-1] != 'gpx':
            continue

        gpx = parse_gpx(gpx_file)
        df = get_df_for_gpx(gpx)
        df['filename'] = gpx_file
        res = res.append(df)

    res.reset_index(drop=True, inplace=True)
    res = res[['filename', 'track_name', 'seg_idx', 'time', 'latitude',
               'longitude', 'elevation']]
    return res

get_df_for_all_files_in(DIR)\
    .to_csv("{}/all_tracks.tsv".format(DIR), sep='\t', index=False)
