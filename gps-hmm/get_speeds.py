#!/usr/bin/env python3
"""
Compute speeds for every track and pickle them into 'speeds.pickle'.
The pickled structure is dict mapping from track name to list of speeds in m/s.
"""

import pickle
import pandas as pd
import gpxpy.gpx as gpx


DIR = "../data"
OUTLIER_THRESHOLD_SPEED = 40


def get_speeds(track_df):
    """Return speeds on the track in m/s."""
    points = track_df.point.tolist()
    prev = points[0]
    res = []
    for cur in points[1:]:
        speed = prev.speed_between(cur)
        if speed < OUTLIER_THRESHOLD_SPEED:
            res.append(speed)
        prev = cur

    return res


df = pd.read_csv("{}/filtered_tracks.tsv".format(DIR), sep='\t')
df["time"] = pd.to_datetime(df.time)
df["point"] = df.apply(lambda row: gpx.GPXTrackPoint(
    row.latitude, row.longitude, row.elevation, row.time), axis=1)

track_to_speeds = {}
for track_name, group in df[['track_name', 'point']].groupby('track_name'):
    track_to_speeds[track_name] = get_speeds(group)

pickle.dump(track_to_speeds, open("{}/speeds.pickle".format(DIR), "wb"))

with open("{}/speeds.txt".format(DIR), "w+") as f:
    for track_name in sorted(track_to_speeds.keys()):
        speeds = track_to_speeds[track_name]
        f.write(track_name + "\n")
        f.write(" ".join(map(str, speeds)))
        f.write("\n")
