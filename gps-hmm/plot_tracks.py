#!/usr/bin/env python3
"""
Plot tracks using google maps. This script generates HTML file for every track.
The data points are hardcoded into HTML files using gmplot library.
The HTML files are saved in HTML_DIR. The zoom level for google maps is
chosen to be ZOOM.
"""

import os
import sys
import gmplot
import logging
import argparse
import pandas as pd


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


ZOOM = 8
DATA_DIR = "../data"
HTML_DIR = "../public"
MODEL_DIR = "../model"
OUTLIER_COLOR = "red"
COLORS = ["green", "blue", "yellow", "white", "magenta", "black"]


parser = argparse.ArgumentParser(
    description="Plot GPS tracks using annotations from HMM.")
parser.add_argument(
    "viterbi_paths", type=str,
    help="Text file with stored Viterbi paths. "
         "Every track is represented by two lines. "
         "First line contains track name. "
         "Second line contains sequence of states.")
parser.add_argument(
    "model_name", type=str,
    help="Name of the model. All HTML files are going to be stores in "
         "../public/model_name/.")
args = parser.parse_args()


def plot_track(track_name, track_df, states):
    """ Plot track on google map

    The track segments are colored based on the state sequence using colors
    in COLORS.

    Args:
        track_name (str): name of the track.
        track_df (pd.DataFrame): data frame with all data points in the track.
        states (list): list of ints - states from HMM.
    """
    latitudes = track_df["latitude"].tolist()
    longitudes = track_df["longitude"].tolist()

    center_lat = (max(latitudes) + min(latitudes)) / 2
    center_long = (max(longitudes) + min(longitudes)) / 2

    gmap = gmplot.GoogleMapPlotter(center_lat, center_long, ZOOM)
    gmap.coloricon = "../markers/%s.png"

    start_seg = 0
    states.append(-1)  # sentinel
    for idx in range(len(states) - 1):
        if states[idx] == states[idx + 1]:
            continue

        color = COLORS[states[idx]] if states[idx] != -1 else OUTLIER_COLOR

        # If we have n states on the segment then the segment has n + 1 nodes.
        lats = latitudes[start_seg:idx + 2]
        longs = longitudes[start_seg:idx + 2]
        gmap.plot(lats, longs, color, edge_width=10)
        gmap.scatter(lats, longs, color=color, marker=True)

        start_seg = idx

    directory = "{}/{}".format(HTML_DIR, args.model_name)
    if not os.path.exists(directory):
        os.makedirs(directory)

    gmap.draw("{}/{}.html".format(directory, track_name.replace(" ", "_")))


df = pd.read_csv("{}/filtered_tracks.tsv".format(DATA_DIR), sep='\t')

with open(args.viterbi_paths, "r") as f:
    while True:
        track_name = f.readline().strip()
        if not track_name:
            break

        states = list(map(int, f.readline().split(" ")))

        logging.info("Plotting '%s' with %d states.", track_name, len(states))
        plot_track(track_name, df[df.track_name == track_name], states)
