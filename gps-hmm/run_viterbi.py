#!/usr/bin/env python3
"""
Load trained HMM and run Viterbi algorithm on all tracks.
The results are saved into text file args.model_file + "_viterbi.txt".
"""

import os
import sys
import pickle
import argparse
import logging

from time import time
from pathos.multiprocessing import ProcessingPool


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


parser = argparse.ArgumentParser(description="Run Viterbi on all tracks.")
parser.add_argument("model_file", type=str, help="Pickled HMM - absolute path.")
parser.add_argument("seqs", type=str,
                    help="Pickled dict mapping from track name to list of values "
                         "which are directly fed into HMM.")
parser.add_argument("outlier_threshold", type=int,
                    help="All value >= are dropped from the sequence and "
                         "are considered as outliers.")
args = parser.parse_args()


hmm = pickle.load(open(args.model_file, "rb"))
track_to_seq = pickle.load(open(args.seqs, "rb"))

seqs = []
for track_name in sorted(track_to_seq.keys()):
    seqs.append([val for val in track_to_seq[track_name]
                 if val < args.outlier_threshold])

pool = ProcessingPool()

logging.info("Running Viterbi...")
start = time()
viterbi_paths = pool.map(hmm.viterbi, seqs)
logging.info("Viterbi took: %f sec.", time() - start)

# Flag all outlier segments by state = -1.
paths = []
for track_name, path in zip(sorted(track_to_seq.keys()), viterbi_paths):
    seq = track_to_seq[track_name]

    pos = 0
    path_with_outliers = []
    for idx in range(len(seq)):
        if seq[idx] < args.outlier_threshold:
            path_with_outliers.append(path[pos])
            pos += 1
        else:
            path_with_outliers.append(-1)

    paths.append(path_with_outliers)

abs_model_path = os.path.abspath(args.model_file)
viterbi_filename = " ".join(abs_model_path.split(".")[:-1]) + "_viterbi.txt"

logging.info("Writing results to text file...")
with open(viterbi_filename, "w+") as f:
    for idx, track_name in enumerate(sorted(track_to_seq.keys())):
        f.write(track_name + "\n")
        f.write(" ".join(map(str, paths[idx])))
        f.write("\n")
