#!/usr/bin/env python3
"""
Filter tracks by the number of track points. Write the output to:
`filtered_tracks.tsv`.
"""

import pandas as pd

THRESHOLD = 100
DIR = "../data"


df = pd.read_csv("{}/all_tracks.tsv".format(DIR), sep='\t')

longer_than_hundred_tracks = df\
  .groupby(['filename', 'track_name', 'seg_idx'])\
  .count()\
  .query('elevation > 100')\
  .reset_index()\
  .track_name

filtered_df = df[df.track_name.isin(longer_than_hundred_tracks)].reset_index()

filtered_df["time"] = pd.to_datetime(filtered_df.time)

filtered_df[["track_name", "time", "latitude", "longitude", "elevation"]]\
  .to_csv('{}/filtered_tracks.tsv'.format(DIR), sep='\t', index=False)
