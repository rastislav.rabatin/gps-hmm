#!/usr/bin/env python3
"""
Train HMM and pickle it into directory MODELS.
"""

import os
import sys
import pickle
import logging
import argparse
import numpy as np

from hmm import ContinuousHMM
from hmm import UnivariateGaussianState
from hmm import ExponentialState


DIR = "../data"
MODELS = "../models"


parser = argparse.ArgumentParser(description="Train HMM.")
parser.add_argument("model_name", type=str, help="Name of the model.")
args = parser.parse_args()


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


if not os.path.exists(MODELS):
    os.makedirs(MODELS)

track_to_speeds = pickle.load(open("{}/speeds.pickle".format(DIR), "rb"))

hmm = ContinuousHMM(transitions=np.array([
    [0.6, 0.399, 0.001, 0, 0], [0.299, 0.4, 0.299, 0.002, 0], [0.002, 0.299, 0.4, 0.299, 0],
    [0, 0.001, 0.399, 0.6, 0], [0, 0, 0.3, 0.4, 0.3]]),
                    emission_distrib=[UnivariateGaussianState(0, 0.2, 0.1),
                                      UnivariateGaussianState(1, 1.4, 0.4),
                                      UnivariateGaussianState(2, 15, 4),
                                      UnivariateGaussianState(3, 26, 6),
                                      UnivariateGaussianState(4, 32, 2)],
                    initial_transitions=np.array([0.4, 0.4, 0.199, 0.0005, 0.0005]))

print("Initial params")
print(hmm)
print("=======================================================================")
print()

seqs = [track_to_speeds[track_name]
        for track_name in sorted(track_to_speeds.keys())]

# One epoch is 5 iterations of EM.
for epoch in range(1, 7):
    logging.info("Epoch: %d", epoch)
    hmm.fit(seqs, n_iters=5)
    hmm.save("{}/{}_iter{}.pickle".format(MODELS, args.model_name, 5*epoch))

print("=======================================================================")
print()
print(hmm)
